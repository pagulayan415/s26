const http = require("http");
const port = 3000
const server = http.createServer((request, response) => {

	if(request.url == '/login'){
		response.writeHead(200, {'Content-type': 'text/plain'})
		response.end('Welcome to the login page.')
	}else{
		response.writeHead(404, {'Content-type': 'text/plain'})
		response.end("I'm sorry the page you are looking for cannot be found")
	}
})

server.listen(port)

// When server is running, console will print the message:
console.log(`Server is successfully running at localhost:${port}`)

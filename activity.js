- What directive is used by Node.js in loading the modules it needs?
	- using require statment.

- What Node.js module contains a method for server creation?
	- http module


- What is the method of the http object responsible for creating a server using Node.js?
	- createServer()

- What method of the response object allows us to set status codes and content types?
	- res.writeHead()

- Where will console.log() output its contents when run in Node.js?
	- in the terminal.

- What property of the request object contains the address's endpoint?
	- res.end()